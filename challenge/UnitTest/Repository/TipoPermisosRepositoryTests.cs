﻿using challenge.Entity;
using challenge.Repository.Data;
using Microsoft.EntityFrameworkCore;

namespace UnitTest.Repository
{
    public class TipoPermisosRepositoryTests
    {
        public static async Task<DataContext> GetDbContext()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
            var databaseContext = new DataContext(options);
            databaseContext.Database.EnsureCreated();
            if (await databaseContext.Permisos.CountAsync() <= 0)
            {
                for(int i = 0; i < 10; i++)
                {
                    databaseContext.TipoPermisos.Add(new TipoPermisos
                    {
                        Descripcion = $"Nombre Tipo Prueba{i}",
                    });
                    await databaseContext.SaveChangesAsync();
                }
            }
            return databaseContext;
        }
    }
}
