﻿using challenge.Entity;
using challenge.Repository.Data;
using Microsoft.EntityFrameworkCore;

namespace UnitTest.Repository
{
    public class PermisosRepositoryTests
    {
        public static async Task<DataContext> GetDbContext()
        {
            var options = new DbContextOptionsBuilder<DataContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;
            var databaseContext = new DataContext(options);
            databaseContext.Database.EnsureCreated();
            if (await databaseContext.Permisos.CountAsync() <= 0)
            {
                for(int i = 0; i < 10; i++)
                {
                    databaseContext.Permisos.Add(new Permisos
                    {
                        NombreEmpleado = $"Nombre Prueba{i}",
                        ApellidoEmpleado = $"Appelido Prueba{i}",
                        TipoPermiso = 1,
                        FechaPermiso = DateTime.Now,
                    });
                    await databaseContext.SaveChangesAsync();
                }
            }
            return databaseContext;
        }
    }
}
