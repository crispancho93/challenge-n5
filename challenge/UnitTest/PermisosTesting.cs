using challenge.Controllers;
using challenge.Entity;
using challenge.Repository;
using challenge.Services;
using Microsoft.AspNetCore.Mvc;
using UnitTest.Repository;

namespace UnitTest
{
    public class PermisosTesting
    {
        [Fact]
        public async void GetAllPermisos()
        {
            // Arrange
            var dbContext = await PermisosRepositoryTests.GetDbContext();
            var repository = new PermisosRepository(dbContext);
            var service = new PermisosService(repository);
            var controller = new PermisosController(service);

            // Act
            var response = await controller.Get();
            var result = response.Result as OkObjectResult;
            var actual = result?.Value as IEnumerable<Permisos>;

            // Asert
            Assert.NotNull(actual);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void GetByIdPermisos()
        {
            // Arrange
            var dbContext = await PermisosRepositoryTests.GetDbContext();
            var repository = new PermisosRepository(dbContext);
            var service = new PermisosService(repository);
            var controller = new PermisosController(service);
            var id = 1;

            // Act
            var response = await controller.Get(id);
            var result = response.Result as OkObjectResult;
            var actual = result?.Value as Permisos;

            // Asert
            Assert.NotNull(actual);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void SaveIdPermisos()
        {
            // Arrange
            var dbContext = await PermisosRepositoryTests.GetDbContext();
            var repository = new PermisosRepository(dbContext);
            var service = new PermisosService(repository);
            var controller = new PermisosController(service);
            var permiso = new Permisos
            {
                NombreEmpleado = "Carlos Emilio",
                ApellidoEmpleado = "Del Socorro",
                FechaPermiso = DateTime.Now,
                TipoPermiso = 1
            };

            // Act
            var response = await controller.Post(permiso);
            var result = response.Result as OkObjectResult;
            var actual = result?.Value as Permisos;

            // Asert
            Assert.NotNull(actual);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void UpdateIdPermisos()
        {
            // Arrange
            var dbContext = await PermisosRepositoryTests.GetDbContext();
            var repository = new PermisosRepository(dbContext);
            var service = new PermisosService(repository);
            var controller = new PermisosController(service);
            var id = 1;
            var permiso = new Permisos
            {
                NombreEmpleado = "Carlos Emilio",
                ApellidoEmpleado = "Del Socorro",
                FechaPermiso = DateTime.Now,
                TipoPermiso = 1
            };

            // Act
            var response = await controller.Put(id, permiso);
            var result = response.Result as OkObjectResult;
            var actual = result?.Value as Permisos;

            // Asert
            Assert.NotNull(actual);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void DeletePermisos()
        {
            // Arrange
            var dbContext = await PermisosRepositoryTests.GetDbContext();
            var repository = new PermisosRepository(dbContext);
            var service = new PermisosService(repository);
            var controller = new PermisosController(service);
            var id = 1;

            // Act
            var response = await controller.Delete(id);
            var result = response.Result as OkObjectResult;
            var actual = result?.Value;

            // Asert
            Assert.NotNull(actual);
            Assert.IsType<OkObjectResult>(result);
        }
    }
}