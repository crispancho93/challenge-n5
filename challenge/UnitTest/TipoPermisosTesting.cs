using challenge.Controllers;
using challenge.Entity;
using challenge.Repository;
using challenge.Services;
using Microsoft.AspNetCore.Mvc;
using UnitTest.Repository;

namespace UnitTest
{
    public class TipoPermisosTesting
    {
        [Fact]
        public async void GetAllPermisos()
        {
            // Arrange
            var dbContext = await TipoPermisosRepositoryTests.GetDbContext();
            var repository = new TipoPermisosRepository(dbContext);
            var service = new TipoPermisosService(repository);
            var controller = new TipoPermisosController(service);

            // Act
            var response = await controller.Get();
            var result = response.Result as OkObjectResult;
            var actual = result?.Value as IEnumerable<TipoPermisos>;

            // Asert
            Assert.NotNull(actual);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void GetByIdPermisos()
        {
            // Arrange
            var dbContext = await TipoPermisosRepositoryTests.GetDbContext();
            var repository = new TipoPermisosRepository(dbContext);
            var service = new TipoPermisosService(repository);
            var controller = new TipoPermisosController(service);
            var id = 1;

            // Act
            var response = await controller.Get(id);
            var result = response.Result as OkObjectResult;
            var actual = result?.Value as TipoPermisos;

            // Asert
            Assert.NotNull(actual);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void SaveIdPermisos()
        {
            // Arrange
            var dbContext = await TipoPermisosRepositoryTests.GetDbContext();
            var repository = new TipoPermisosRepository(dbContext);
            var service = new TipoPermisosService(repository);
            var controller = new TipoPermisosController(service);
            var permiso = new TipoPermisos
            {
                Descripcion = "Carlos Emilio",
            };

            // Act
            var response = await controller.Post(permiso);
            var result = response.Result as OkObjectResult;
            var actual = result?.Value as TipoPermisos;

            // Asert
            Assert.NotNull(actual);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void UpdateIdPermisos()
        {
            // Arrange
            var dbContext = await TipoPermisosRepositoryTests.GetDbContext();
            var repository = new TipoPermisosRepository(dbContext);
            var service = new TipoPermisosService(repository);
            var controller = new TipoPermisosController(service);
            var id = 1;
            var permiso = new TipoPermisos
            {
                Descripcion = "System administrator",
            };

            // Act
            var response = await controller.Put(id, permiso);
            var result = response.Result as OkObjectResult;
            var actual = result?.Value as TipoPermisos;

            // Asert
            Assert.NotNull(actual);
            Assert.IsType<OkObjectResult>(result);
        }

        [Fact]
        public async void DeletePermisos()
        {
            // Arrange
            var dbContext = await TipoPermisosRepositoryTests.GetDbContext();
            var repository = new TipoPermisosRepository(dbContext);
            var service = new TipoPermisosService(repository);
            var controller = new TipoPermisosController(service);
            var id = 1;

            // Act
            var response = await controller.Delete(id);
            var result = response.Result as OkObjectResult;
            var actual = result?.Value;

            // Asert
            Assert.NotNull(actual);
            Assert.IsType<OkObjectResult>(result);
        }
    }
}