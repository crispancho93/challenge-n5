using challenge.Repository;
using challenge.Repository.Data;
using challenge.Repository.Interfaces;
using challenge.Services;
using challenge.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
// Repository
builder.Services.AddScoped<IPermisosRepository, PermisosRepository>();
builder.Services.AddScoped<ITipoPermisosRepository, TipoPermisosRepository>();
builder.Services.AddScoped<IPermisosService, PermisosService>();
builder.Services.AddScoped<ITipoPermisosService, TipoPermisosService>();
// Add EF
builder.Services.AddDbContext<DataContext>(
    options => options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));
//services cors
builder.Services.AddCors(p => p.AddPolicy("corsapp", builder =>
{
    builder.WithOrigins("*").AllowAnyMethod().AllowAnyHeader();
}));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors("corsapp");

app.UseAuthorization();

app.MapControllers();

app.Run();
