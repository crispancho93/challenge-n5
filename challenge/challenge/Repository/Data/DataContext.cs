﻿using challenge.Entity;
using Microsoft.EntityFrameworkCore;

namespace challenge.Repository.Data
{
    public class DataContext : DbContext
    {
        public DbSet<Permisos> Permisos { get; set; } = null!;
        public DbSet<TipoPermisos> TipoPermisos { get; set; } = null!;
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }
    }
}
