﻿using challenge.DTO;
using challenge.Entity;
using challenge.Repository.Data;
using challenge.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace challenge.Repository
{
    public class PermisosRepository : IPermisosRepository
    {
        private readonly DataContext _context;

        public PermisosRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// GetAll
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<PermisosDTO>> GetAll()
        {
            var query =  await 
            (
                from p in _context.Permisos
                join t in _context.TipoPermisos on p.TipoPermiso equals t.Id
                select new PermisosDTO
                {
                    Id = p.Id,
                    NombreEmpleado = p.NombreEmpleado,
                    ApellidoEmpleado = p.ApellidoEmpleado,
                    TipoPermiso = p.TipoPermiso,
                    DescripcionTipoPermiso = t.Descripcion,
                    FechaPermiso = p.FechaPermiso
                }
            ).ToListAsync();
            return query;

        }

        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Permisos?> GetById(int id)
        {
            var permiso = await _context.Permisos.FindAsync(id);
            return permiso;
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="permiso"></param>
        /// <returns></returns>
        public async Task<Permisos> Save(Permisos permiso)
        {
            _context.Add(permiso);
            await _context.SaveChangesAsync();
            return permiso;
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="permiso"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task<Permisos> Update(Permisos permiso)
        {
            var dbPermiso = await _context.Permisos.FindAsync(permiso.Id);

            if (dbPermiso is null)
            {
                throw new Exception("Data NotFound");
            }

            dbPermiso.NombreEmpleado = permiso.NombreEmpleado;
            dbPermiso.ApellidoEmpleado = permiso.ApellidoEmpleado;
            dbPermiso.TipoPermiso = permiso.TipoPermiso;
            dbPermiso.FechaPermiso = permiso.FechaPermiso;

            await _context.SaveChangesAsync();
            return permiso;

        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task<int> Delete(int id)
        {
            var dbPermiso = await _context.Permisos.FindAsync(id);

            if (dbPermiso is null)
            {
                throw new Exception("Data NotFound");
            }

            _context.Remove(dbPermiso);
            return await _context.SaveChangesAsync();
        }

    }
}
