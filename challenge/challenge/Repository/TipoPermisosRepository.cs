﻿using challenge.Entity;
using challenge.Repository.Data;
using challenge.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace challenge.Repository
{
    public class TipoPermisosRepository : ITipoPermisosRepository
    {
        private readonly DataContext _context;

        public TipoPermisosRepository(DataContext context)
        {
            _context = context;
        }

        /// <summary>
        /// GetAll
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<TipoPermisos>> GetAll()
        {
            var tipoPermisos = await _context.TipoPermisos.ToListAsync();
            return tipoPermisos;
        }

        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<TipoPermisos?> GetById(int id)
        {
            var tipoPermiso = await _context.TipoPermisos.FindAsync(id);
            return tipoPermiso;
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="tipoPermiso"></param>
        /// <returns></returns>
        public async Task<TipoPermisos> Save(TipoPermisos tipoPermiso)
        {
            _context.Add(tipoPermiso);
            await _context.SaveChangesAsync();
            return tipoPermiso;
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="tipoPermiso"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task<TipoPermisos> Update(TipoPermisos tipoPermiso)
        {
            var dbTipoPermiso = await _context.TipoPermisos.FindAsync(tipoPermiso.Id);

            if (dbTipoPermiso is null)
            {
                throw new Exception("Data NotFound");
            }

            dbTipoPermiso.Descripcion = tipoPermiso.Descripcion;

            await _context.SaveChangesAsync();
            return tipoPermiso;

        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public async Task<int> Delete(int id)
        {
            var dbTipoPermiso = await _context.TipoPermisos.FindAsync(id);

            if (dbTipoPermiso is null)
            {
                throw new Exception("Data NotFound");
            }

            _context.Remove(dbTipoPermiso);
            return await _context.SaveChangesAsync();
        }

    }
}
