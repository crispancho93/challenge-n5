﻿using challenge.DTO;
using challenge.Entity;

namespace challenge.Repository.Interfaces
{
    public interface IPermisosRepository
    {
        Task<IEnumerable<PermisosDTO>> GetAll();
        Task<Permisos?> GetById(int id);
        Task<Permisos> Save(Permisos permiso);
        Task<Permisos> Update(Permisos permiso);
        Task<int> Delete(int id);
    }
}
