﻿using challenge.Entity;

namespace challenge.Repository.Interfaces
{
    public interface ITipoPermisosRepository
    {
        Task<IEnumerable<TipoPermisos>> GetAll();
        Task<TipoPermisos?> GetById(int id);
        Task<TipoPermisos> Save(TipoPermisos tipoPermiso);
        Task<TipoPermisos> Update(TipoPermisos tipoPermiso);
        Task<int> Delete(int id);
    }
}
