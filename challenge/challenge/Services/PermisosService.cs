﻿using challenge.DTO;
using challenge.Entity;
using challenge.Repository.Interfaces;
using challenge.Services.Interfaces;

namespace challenge.Services
{
    public class PermisosService : IPermisosService
    {
        private readonly IPermisosRepository _repository;

        public PermisosService(IPermisosRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// GetAll
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<PermisosDTO>> GetAll()
        {
            // TODO: other logic for this layer
            return await _repository.GetAll();
        }

        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Permisos?> GetById(int id)
        {
            // TODO: other logic for this layer
            return await _repository.GetById(id);
        }

        
        /// <summary>
        /// Save
        /// </summary>
        /// <param name="permiso"></param>
        /// <returns></returns>
        public async Task<Permisos> Save(Permisos permiso)
        {
            // TODO: other logic for this layer
            return await _repository.Save(permiso);
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="permiso"></param>
        /// <returns></returns>
        public async Task<Permisos> Update(Permisos permiso)
        {
            // TODO: other logic for this layer
            return await _repository.Update(permiso);
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<int> Delete(int id)
        {
            // TODO: other logic for this layer
            return await _repository.Delete(id);
        }
    }
}
