﻿using challenge.Entity;

namespace challenge.Services.Interfaces
{
    public interface ITipoPermisosService
    {
        Task<IEnumerable<TipoPermisos>> GetAll();
        Task<TipoPermisos?> GetById(int id);
        Task<TipoPermisos> Save(TipoPermisos tipoPermiso);
        Task<TipoPermisos> Update(TipoPermisos tipoPermiso);
        Task<int> Delete(int id);
    }
}
