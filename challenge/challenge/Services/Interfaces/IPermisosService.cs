﻿using challenge.DTO;
using challenge.Entity;

namespace challenge.Services.Interfaces
{
    public interface IPermisosService
    {
        Task<IEnumerable<PermisosDTO>> GetAll();
        Task<Permisos?> GetById(int id);
        Task<Permisos> Save(Permisos permiso);
        Task<Permisos> Update(Permisos permiso);
        Task<int> Delete(int id);
    }
}
