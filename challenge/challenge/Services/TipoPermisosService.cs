﻿using challenge.Entity;
using challenge.Repository.Interfaces;
using challenge.Services.Interfaces;

namespace challenge.Services
{
    public class TipoPermisosService : ITipoPermisosService
    {
        private readonly ITipoPermisosRepository _repository;

        public TipoPermisosService(ITipoPermisosRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// GetAll
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<TipoPermisos>> GetAll()
        {
            // TODO: other logic for this layer
            return await _repository.GetAll();
        }

        /// <summary>
        /// GetById
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<TipoPermisos?> GetById(int id)
        {
            // TODO: other logic for this layer
            return await _repository.GetById(id);
        }

        /// <summary>
        /// Save
        /// </summary>
        /// <param name="tipoPermiso"></param>
        /// <returns></returns>
        public async Task<TipoPermisos> Save(TipoPermisos tipoPermiso)
        {
            // TODO: other logic for this layer
            return await _repository.Save(tipoPermiso);
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="tipoPermiso"></param>
        /// <returns></returns>
        public async Task<TipoPermisos> Update(TipoPermisos tipoPermiso)
        {
            // TODO: other logic for this layer
            return await _repository.Update(tipoPermiso);
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<int> Delete(int id)
        {
            // TODO: other logic for this layer
            return await _repository.Delete(id);
        }

    }
}
