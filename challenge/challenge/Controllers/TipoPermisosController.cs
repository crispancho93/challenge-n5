﻿using challenge.Entity;
using challenge.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace challenge.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TipoPermisosController : ControllerBase
    {
        private readonly ITipoPermisosService _service;

        public TipoPermisosController(ITipoPermisosService service)
        {
            _service = service;
        }

        // GET: api/<TipoPermisosController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TipoPermisos>>> Get()
        {
            try
            {
                var tipoPermiso = await _service.GetAll();
                return Ok(tipoPermiso);
            }
            catch (Exception ex)
            {
                return BadRequest(new { msg = ex.Message });
            }
        }

        // GET api/<TipoPermisosController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TipoPermisos>> Get(int id)
        {
            try
            {
                var tipoPermiso = await _service.GetById(id);

                if (tipoPermiso is null)
                {
                    return NotFound(new { msg = "Data NotFound" });
                }

                return Ok(tipoPermiso);
            }
            catch (Exception ex)
            {
                return BadRequest(new { msg = ex.Message });
            }
        }

        // POST api/<TipoPermisosController>
        [HttpPost]
        public async Task<ActionResult<TipoPermisos>> Post([FromBody] TipoPermisos tipoPermiso)
        {
            try
            {
                var dbTipoPermiso = await _service.Save(tipoPermiso);
                return Ok(dbTipoPermiso);
            }
            catch (Exception ex)
            {
                return BadRequest(new { msg = ex.Message });
            }
        }

        // PUT api/<TipoPermisosController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult<TipoPermisos>> Put(int id, [FromBody] TipoPermisos tipoPermiso)
        {
            try
            {
                tipoPermiso.Id = id;
                var dbPermiso = await _service.Update(tipoPermiso);
                return Ok(dbPermiso);
            }
            catch (Exception ex)
            {
                if (ex.Message is "Data NotFound")
                    return NotFound(new { msg = "Data NotFound" });
                else
                    return BadRequest(new { msg = ex.Message });
            }
        }

        // DELETE api/<TipoPermisosController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<int>> Delete(int id)
        {
            try
            {
                var result = await _service.Delete(id);
                return Ok(new { msg = "Delete Successfully" });
            }
            catch (Exception ex)
            {
                if (ex.Message is "Data NotFound")
                    return NotFound(new { msg = "Data NotFound" });
                else
                    return BadRequest(new { msg = ex.Message });
            }

        }
    }
}
