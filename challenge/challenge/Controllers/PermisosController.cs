﻿using challenge.DTO;
using challenge.Entity;
using challenge.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace challenge.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PermisosController : ControllerBase
    {
        private readonly IPermisosService _service;

        public PermisosController(IPermisosService service)
        {
            _service = service;
        }

        // GET: api/<PermisosController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PermisosDTO>>> Get()
        {
            try
            {
                var permisos = await _service.GetAll();
                return Ok(permisos);
            }
            catch (Exception ex)
            {
                return BadRequest(new { msg = ex.Message });
            }
        }

        // GET api/<PermisosController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Permisos>> Get(int id)
        {
            try
            {
                var permiso = await _service.GetById(id);

                if (permiso is null)
                {
                    return NotFound(new { msg = "Data NotFound" });
                }

                return Ok(permiso);
            }
            catch (Exception ex)
            {
                return BadRequest(new { msg = ex.Message });
            }
        }

        // POST api/<PermisosController>
        [HttpPost]
        public async Task<ActionResult<Permisos>> Post([FromBody] Permisos permiso)
        {
            try
            {
                var dbPermiso = await _service.Save(permiso);
                return Ok(dbPermiso);
            }
            catch (Exception ex)
            {
                return BadRequest(new { msg = ex.Message });
            }
        }

        // PUT api/<PermisosController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult<Permisos>> Put(int id, [FromBody] Permisos permiso)
        {
            try
            {
                permiso.Id = id;
                var dbPermiso = await _service.Update(permiso);
                return Ok(dbPermiso);
            }
            catch (Exception ex)
            {
                if (ex.Message is "Data NotFound")
                    return NotFound(new { msg = "Data NotFound" });
                else
                    return BadRequest(new { msg = ex.Message });
            }
        }

        // DELETE api/<PermisosController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<int>> Delete(int id)
        {
            try
            {
                var result = await _service.Delete(id);
                return Ok(new { msg = "Delete Successfully" });
            }
            catch (Exception ex)
            {
                if (ex.Message is "Data NotFound")
                    return NotFound(new { msg = "Data NotFound" });
                else
                    return BadRequest(new { msg = ex.Message });
            }
        }
    }
}
