﻿using System.ComponentModel.DataAnnotations.Schema;

namespace challenge.Entity
{
    [Table("TipoPermisos")]
    public class TipoPermisos
    {
        public int Id { get; set; }
        public string Descripcion { get; set; } = null!;
    }
}
