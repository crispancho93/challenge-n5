﻿using System.ComponentModel.DataAnnotations.Schema;

namespace challenge.Entity
{
    [Table("Permisos")]
    public class Permisos
    {
        public int Id { get; set; }
        public string NombreEmpleado { get; set; } = null!;
        public string ApellidoEmpleado { get; set; } = null!;
        public int TipoPermiso { get; set; }
        public DateTime FechaPermiso { get; set; }
    }
}
