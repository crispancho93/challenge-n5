IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = 'challenge')
BEGIN
CREATE DATABASE challenge
END
GO
    USE challenge
GO
DROP TABLE IF EXISTS TipoPermisos
CREATE TABLE TipoPermisos (
	Id INTEGER IDENTITY(1,1) PRIMARY KEY,
	Descripcion VARCHAR(45) NOT NULL
)
GO
DROP TABLE IF EXISTS Permisos
CREATE TABLE Permisos (
	Id INTEGER IDENTITY(1,1) PRIMARY KEY,
	NombreEmpleado VARCHAR(45) NOT NULL,
	ApellidoEmpleado VARCHAR(45) NOT NULL,
	TipoPermiso INT NOT NULL,
	FechaPermiso DATE NOT NULL,

	CONSTRAINT TipoPermiso FOREIGN KEY(TipoPermiso) REFERENCES TipoPermisos(id)
)
GO
INSERT INTO TipoPermisos (Descripcion) VALUES
('Brand Manager')
GO
INSERT INTO Permisos (NombreEmpleado, ApellidoEmpleado, TipoPermiso, FechaPermiso) VALUES
('Cristian', 'Arboleda', 1, '2022-08-22'),
('Julieta', 'Arboleda', 1, '2022-08-22'),
('Daniel', 'Arboleda', 1, '2022-08-22'),
('Sarah', 'Arboleda', 1, '2022-08-22'),
('Helena Guzman', 'Arboleda', 1, '2022-08-22')