import request from 'axios';

/**
 * 
 * @param {*} action 
 * @param {*} method 
 * @param {*} data 
 * @returns 
 */
const sendServer = async (action, method, data) => {
  const result = await request({
    method,
    url: `${process.env.REACT_APP_BASE_URL}/${action}`,
    data,
    headers: {
      "Content-Type": "application/json",
    },
  })
  return result?.data;
};

export { 
  sendServer
}
