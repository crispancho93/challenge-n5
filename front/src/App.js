import 'antd/dist/antd.min.css';
import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Shared from './components/Shared';
import Permisos from './components/Permisos';
import TipoPermisos from './components/TipoPermisos';

function App() {
  return (
    <Router>
      <Routes>
        <Route path='/' element={<Shared component={Permisos} />} />
        <Route path='/tipo-permisos' element={<Shared component={TipoPermisos} />} />
      </Routes>
    </Router>
  );
}

export default App;
