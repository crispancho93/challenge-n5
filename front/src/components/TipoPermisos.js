import React, { useEffect, useState } from 'react';
import { Table, Button, Row, Col, Space, Modal, Form, Input } from 'antd';
import { ArrowLeftOutlined, PlusSquareOutlined, ReloadOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
import { sendServer } from '../utils/utils'


const TipoPermisos = () => {

  const columns = [
    {
      title: 'Descripción',
      dataIndex: 'descripcion',
    },
    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <a onClick={() => handleEdit(record)}>Editar</a>
          <a onClick={() => handleDelete(record)}>Eliminar</a>
        </Space>
      ),
    },
  ];

  const navigate = useNavigate();
  const [form] = Form.useForm();
  const [data, setData] = useState([]);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    loadData();
    return () => { }
  }, [])

  const loadData = () => {

    setData([]);
    sendServer('tipoPermisos', 'GET', {})
      .then(d => {
        setData([...d]);
      })
      .catch(e => {
        alert(`¡Ups ${e}!`);
      });
  }

  const onFinish = (values) => {
    if (values.id) {
      sendServer(`tipoPermisos/${values.id}`, 'PUT', values)
        .then(d => {

          alert('Editado correctamente!');
          loadData();
          setVisible(false);
          form.resetFields();

        }).catch(e => {
          alert(`¡Ups ${e}!`);
        });
    } else {
      sendServer('tipoPermisos', 'POST', values)
        .then(d => {

          alert('¡Agregado correctamente!');
          loadData();
          setVisible(false);
          form.resetFields();

        }).catch(e => {
          alert(`¡Ups ${e}!`);
        });
    }
  };

  const handleEdit = (values) => {
    form.setFieldsValue({ ...values });
    setVisible(true);
  }

  const handleDelete = (values) => {
    const flag = window.confirm("¿Desea eliminar, esta acción no se puede revertir?");
    if (flag) {
      sendServer(`tipoPermisos/${values.id}`, 'DELETE', {})
        .then(d => {

          alert('Eliminado correctamente!');
          loadData();
          setVisible(false);
          form.resetFields();

        }).catch(e => {
          alert(`¡Ups ${e}!`);
        });
    }
  }

  const handleClose = () => {
    setVisible(false);
    form.resetFields();
  }

  return (
    <div style={{ marginTop: 40, textAlign: 'center' }}>
      <h2>TIPO PERMISOS</h2>
      <Row style={{ marginBottom: 10 }}>
        <Col>
          <Button onClick={() => navigate(-1)}><ArrowLeftOutlined /></Button>
        </Col>
        <Col style={{ marginLeft: 'auto', marginRight: 2 }}>
          <Button type='default' onClick={() => { setVisible(true) }}><PlusSquareOutlined /></Button>
          <Button type='default' onClick={loadData} style={{ marginLeft: 6 }}><ReloadOutlined /></Button>
        </Col>
      </Row>
      <Table
        rowKey={'id'}
        columns={columns}
        dataSource={data}
        scroll={{ x: 500, y: 300 }}
        size={'middle'}
      />

      <Modal
        title="Tipo Permisos"
        centered
        visible={visible}
        onOk={form.submit}
        onCancel={handleClose}
        width={1000}
      >
        <Form
          form={form}
          name="frmTipoPermisos"
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item
            name="id"
            rules={[
              { required: false },
            ]}
            hidden={true}
          >
            <Input />
          </Form.Item>
          <Row gutter="24">
            <Col xs={24} sm={24} md={24}>
              <Form.Item
                labelCol={{ span: 24 }}
                label="Descripción"
                name="descripcion"
                rules={[
                  {
                    required: true,
                    message: 'Please input your Descripción!',
                  },
                ]}
              >
                <Input />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
    </div>
  )
}

export default TipoPermisos;