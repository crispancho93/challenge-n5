import React, { useEffect, useState } from 'react';
import { Table, Button, Row, Col, Tag, Space, Modal, Form, Input, Select } from 'antd';
import { ArrowLeftOutlined, PlusSquareOutlined, ReloadOutlined } from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
import { sendServer } from '../utils/utils'

const { Option } = Select;

const Permisos = () => {

  const columns = [
    {
      title: 'Nombre',
      dataIndex: 'nombreEmpleado',
    },
    {
      title: 'Apellido',
      dataIndex: 'apellidoEmpleado',
    },
    {
      title: 'Permiso',
      dataIndex: 'descripcionTipoPermiso',
    },
    {
      title: 'Fecha permiso',
      dataIndex: 'fechaPermiso',
      render: (text) => <Tag color="green">{text?.split('T')[0]}</Tag>
    },
    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Space size="middle">
          <a onClick={() => handleEdit(record)}>Editar</a>
          <a onClick={() => handleDelete(record)}>Eliminar</a>
        </Space>
      ),
    },
  ];

  const navigate = useNavigate();
  const [form] = Form.useForm();
  const [data, setData] = useState([]);
  const [tipos, setTipos] = useState([]);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    loadData();
    return () => { }
  }, [])

  const loadData = () => {

    setData([]);
    sendServer('permisos', 'GET', {})
      .then(d => {
        setData([...d]);
      })
      .catch(e => {
        alert(`¡Ups ${e}!`);
      });

    sendServer('tipoPermisos', 'GET', {})
      .then(d => {
        setTipos([...d]);
      })
      .catch(e => {
        alert(`¡Ups ${e}!`);
      });
  }

  const onFinish = (values) => {
    if (values.id) {
      sendServer(`permisos/${values.id}`, 'PUT', values)
        .then(d => {

          alert('Editado correctamente!');
          loadData();
          setVisible(false);
          form.resetFields();

        }).catch(e => {
          alert(`¡Ups ${e}!`);
        });
    } else {
      sendServer('permisos', 'POST', values)
        .then(d => {

          alert('¡Agregado correctamente!');
          loadData();
          setVisible(false);
          form.resetFields();

        }).catch(e => {
          alert(`¡Ups ${e}!`);
        });
    }
  };

  const handleEdit = (values) => {
    form.setFieldsValue({
      ...values,
      fechaPermiso: values.fechaPermiso?.split('T')[0]
    });
    setVisible(true);
  }

  const handleDelete = (values) => {
    const flag = window.confirm("¿Desea eliminar, esta acción no se puede revertir?");
    if (flag) {
      sendServer(`permisos/${values.id}`, 'DELETE', {})
        .then(d => {

          alert('Eliminado correctamente!');
          loadData();
          setVisible(false);
          form.resetFields();

        }).catch(e => {
          alert(`¡Ups ${e}!`);
        });
    }
  }

  const handleClose = () => {
    setVisible(false);
    form.resetFields();
  }

  return (
    <div style={{ marginTop: 40, textAlign: 'center' }}>
      <h2>PERMISOS</h2>
      <Row style={{ marginBottom: 10 }}>
        <Col>
          <Button onClick={() => navigate(-1)}><ArrowLeftOutlined /></Button>
        </Col>
        <Col style={{ marginLeft: 'auto', marginRight: 2 }}>
          <Button type='default' onClick={() => { setVisible(true) }}><PlusSquareOutlined /></Button>
          <Button type='default' onClick={ loadData } style={{ marginLeft: 6 }}><ReloadOutlined /></Button>
        </Col>
      </Row>
      <Table
        rowKey={'id'}
        columns={columns}
        dataSource={data}
        scroll={{ x: 500, y: 300 }}
        size={'middle'}
      />

      <Modal
        title="Permiso"
        centered
        visible={visible}
        onOk={form.submit}
        onCancel={handleClose}
        width={1000}
      >
        <Form
          form={form}
          name="frmPermisos"
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item
            name="id"
            rules={[
              { required: false },
            ]}
            hidden={true}
          >
            <Input />
          </Form.Item>
          <Row gutter="24">
            <Col xs={24} sm={24} md={12}>
              <Form.Item
                labelCol={{ span: 24 }}
                label="Nombre"
                name="nombreEmpleado"
                rules={[
                  {
                    required: true,
                    message: 'Please input your Nombre!',
                  },
                ]}
              >
                <Input />
              </Form.Item>
            </Col>

            <Col xs={24} sm={24} md={12}>
              <Form.Item
                labelCol={{ span: 24 }}
                label="Apellido"
                name="apellidoEmpleado"
                rules={[
                  {
                    required: true,
                    message: 'Please input your Apellido!',
                  },
                ]}
              >
                <Input />
              </Form.Item>
            </Col>

            <Col xs={24} sm={24} md={12}>
              <Form.Item
                labelCol={{ span: 24 }}
                label="Tipo permiso"
                name="tipoPermiso"
                rules={[
                  {
                    required: true,
                    message: 'Please input your Tipo permiso!',
                  },
                ]}
              >
                <Select
                  placeholder="Tipo permiso"
                >
                  {
                    tipos.map(op => (
                      <Option key={op.id} value={op.id}>
                        {op.descripcion}
                      </Option>
                    ))
                  }
                </Select>
              </Form.Item>
            </Col>

            <Col xs={24} sm={24} md={12}>
              <Form.Item
                labelCol={{ span: 24 }}
                label="Fecha"
                name="fechaPermiso"
                rules={[
                  {
                    required: true,
                    message: 'Please input your Fecha!',
                  },
                ]}
              >
                <Input type='date' />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
    </div>
  )
}

export default Permisos;