import React, { useEffect, useState } from 'react';
import { Menu } from 'antd';
import { MailOutlined } from '@ant-design/icons';
import { useNavigate, useLocation } from 'react-router-dom';

const Shared = ({ component: Component }) => {

    const navigate = useNavigate();
    const location = useLocation();
    const [tab, setTab] = useState(undefined);

    useEffect(() => {
        if (location.pathname == '/')
            setTab('permisos');
        else
            setTab('tipo-permisos');
        return () => { }
    }, [location.pathname])


    return (
        <>
            <Menu mode="horizontal" selectedKeys={tab}>
                <Menu.Item key="permisos" icon={<MailOutlined />} onClick={() => navigate('/')}>
                    Permisos
                </Menu.Item>
                <Menu.Item key="tipo-permisos" icon={<MailOutlined />} onClick={() => navigate('/tipo-permisos')}>
                    Tipos
                </Menu.Item>
            </Menu>
            <div style={{ padding: 20 }}>
                <Component />
            </div>
        </>
    )
}

export default Shared;